# Unity Bullet Penetration (Unity 2019.4 URP)

This project is a simple prototype on FPS style games where bullets can go through walls and damage targets on the other side. The damage is a set value then subtracted by the thickness of what part it passed through. This project is best viewed inside of the editor as it uses gizmos to show the points.

![Gif through targets](https://gitlab.com/ben-w/unity-bullet-penetration/-/raw/master/Videos/Gif.gif)

![Video through walls](https://gitlab.com/ben-w/unity-bullet-penetration/-/raw/master/Videos/Video.mp4)

## What it started as
When I started this project, I was trying to affect the velocity of the bullet as well however, I realised the problem later down the line that this would be way harder. So instead the bullets do not drop with gravity but lose damage through each wall.

![Gif of what it first started out as](https://gitlab.com/ben-w/unity-bullet-penetration/-/raw/master/Videos/oldgif.gif)
*My first attempt at this*

## How it works
It works by raycasting between the last frame and the current frame, rigid body position. If there is only one wall in between those two, it sends a raycast back from its current position then uses that hit point and the first hit point to work out the thickness. If there are multiply walls, it will put them in order and then send raycasts back to the previous wall to work out the distance. Lastly, if the object is inside a wall during the frame. It will try to send a raycast forward to see if there is anything to bounce off of, if that fails, it sends a ray back from a far distance away.

**The crosshair isn't where you shoot, its just the centre of the screen**
