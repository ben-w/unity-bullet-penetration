﻿interface IDamagable
{
    void ApplyDamage(float amount);
}