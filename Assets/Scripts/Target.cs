﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Target : MonoBehaviour, IDamagable
{
    [SerializeField]
    private TextMeshPro _text;
    [SerializeField]
    private Transform _textHolder;
    [SerializeField]
    private AudioClip _hitSound;
    [SerializeField]
    private AudioSource _soundSource;
    [SerializeField]
    private Transform _player;

    public void ApplyDamage(float amount)
    {
        // Take away health
        PlayHitEffect(amount);
        UpdateText(amount);
    }

    private void PlayHitEffect(float amount)
    {
        _soundSource.pitch = amount / 100f;
        _soundSource.PlayOneShot(_hitSound);
    }
    private void UpdateText(float lastDamage)
    {
        _text.text = lastDamage.ToString();
    }
    private void LateUpdate()
    {
        _textHolder.transform.LookAt(_player);
    }
}
