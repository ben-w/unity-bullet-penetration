﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class Bullet : MonoBehaviour
{
    [SerializeField]
    private Vector3 _lastPosition;
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private float _slowdownAmount;
    [SerializeField]
    private float _pauseTimer = 5f;
    [SerializeField]
    private float _destoryTimer = 10f;
    [SerializeField]
    private float _damage = 100f;

    private List<Vector3> _gizmoHitPoints = new List<Vector3>();
    private List<Vector3> _gizmoLines = new List<Vector3>();
    private RaycastHit[] _lastHits;
    private float _lastDistance;
    private bool _isStopped;
    private void Start()
    {
        _lastPosition = _rigidbody.position;
        _gizmoLines.Add(_rigidbody.position);
        StartCoroutine(StopTimer());
    }
    private void LateUpdate()
    {
        if (_isStopped)
            return;
        _lastDistance = Vector3.Distance(_rigidbody.position, _lastPosition);
        _lastHits = Physics.RaycastAll(_lastPosition, _rigidbody.position - _lastPosition, _lastDistance);

        if (_lastHits.Length > 0)
            CheckThickness(_lastHits);

        _lastPosition = _rigidbody.position;
        _gizmoLines.Add(_rigidbody.position);
    }
    private void HitObject(float thickness, Vector3 outPoint, Vector3 inPoint, Transform target)
    {
        IDamagable damagable = target.GetComponent<IDamagable>();
        if (damagable != null)
            damagable.ApplyDamage(_damage);

        _gizmoHitPoints.Add(inPoint);
        _gizmoHitPoints.Add(outPoint);

        _damage -= thickness * _slowdownAmount;


        if (DestroyCheck())
        {
            _rigidbody.position = inPoint;
            return;
        }
        _rigidbody.position = outPoint;
    }
    private void HitObject(Vector3 inPoint, Transform transform)
    {
        // If we are inside an object and can't find a wall 
        IDamagable damagable = transform.GetComponent<IDamagable>();
        if (damagable != null)
            damagable.ApplyDamage(_damage);

        _gizmoHitPoints.Add(inPoint);
        _damage -= transform.localScale.x * _slowdownAmount;

        DestroyCheck();
    }
    private bool DestroyCheck()
    {
        if (_damage <= 0)
        {
            StopBullet();
            return true;
        }
        return false;
    }
    private void CheckThickness(RaycastHit[] hit)
    {
        if (hit.Length == 1)
        {
            bool foundWall = Physics.Raycast(_rigidbody.position, _lastPosition - _rigidbody.position, out RaycastHit hitinfo, _lastDistance);
            if (!foundWall)
            {
                InsideCheck(hit[0]);
                return;
            }
            float distance = Vector3.Distance(hit[0].point, hitinfo.point);
            HitObject(distance, hitinfo.point, hit[0].point, hit[0].transform);
            return;
        }

        List<RaycastHit> hitsInOrder = hit.OrderBy(h => h.distance).ToList();
        Vector3 startPoint;
        for (int i = 0; i < hitsInOrder.Count; i++)
        {
            if (_isStopped)
                return;
            if (i == hitsInOrder.Count - 1)
                startPoint = _rigidbody.position;
            else
                startPoint = hitsInOrder[i + 1].point;

            bool isFound = Physics.Raycast(startPoint, hitsInOrder[i].point - startPoint, out RaycastHit hitinfo, _lastDistance);
            if (!isFound)
            {
                StopBullet();
                return;
            }
            float distance = Vector3.Distance(hitsInOrder[i].point, hitinfo.point);
            HitObject(distance, hitinfo.point, hitsInOrder[i].point, hitsInOrder[i].transform);
            Debug.Break();
        }
    }
    private void StopBullet()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.isKinematic = true;
        _isStopped = true;
        Destroy(gameObject, _destoryTimer);
    }
    private void InsideCheck(RaycastHit entry)
    {
        //We first look for another wall to bounce off
        bool hasAnotherWall = Physics.Raycast(_rigidbody.position, transform.forward, out RaycastHit somehit);
        if (hasAnotherWall)
        {
            if (Physics.Raycast(somehit.point, _rigidbody.position - somehit.point, out RaycastHit end))
            {
                _gizmoHitPoints.Add(end.point);

                float distance = Vector3.Distance(entry.point, end.point);
                HitObject(distance, end.point, entry.point, entry.transform);
                return;
            }
        }

        // We try sending a ray back to us from really really far away
        Vector3 farForward = transform.TransformPoint(Vector3.forward * 30000);
        bool foundWall = Physics.Raycast(farForward, _rigidbody.position - farForward, out RaycastHit ourWall);
        if (foundWall)
        {
            float distance = Vector3.Distance(entry.point, ourWall.point);
            HitObject(distance, ourWall.point, entry.point, entry.transform);
            return;
        }

        // If all else fails, we just use the scale of the object.
        HitObject(entry.point, entry.collider.transform);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < _gizmoHitPoints.Count; i++)
        {
            Gizmos.DrawSphere(_gizmoHitPoints[i], 0.05f);
        }

        Gizmos.color = Color.green;
        for (int i = 0; i < _gizmoLines.Count; i++)
        {
            if (i == 0)
                continue;
            Gizmos.DrawLine(_gizmoLines[i - 1], _gizmoLines[i]);
        }
    }
    private IEnumerator StopTimer()
    {
        yield return new WaitForSeconds(_pauseTimer);
        StopBullet();
    }
}
