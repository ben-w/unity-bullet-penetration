﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject BulletPrefab;
    public Transform SpawnLocation;
    public Vector3 bulletSpeed;
    public AudioSource AudioSource;
    public AudioClip AudioClip;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Transform bullet = Instantiate(BulletPrefab).transform;
            Rigidbody rigidbody = bullet.GetComponent<Rigidbody>();
            rigidbody.position = SpawnLocation.position;
            rigidbody.rotation = SpawnLocation.rotation;

            bullet.position = SpawnLocation.position;
            bullet.rotation = SpawnLocation.rotation;
            rigidbody.AddForce(bullet.TransformDirection(bullet.forward + bulletSpeed));
            AudioSource.PlayOneShot(AudioClip);
        }
    }
}
